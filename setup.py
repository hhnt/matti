from setuptools import setup,find_packages

setup(
        name='matti',
        version='0.1',
        description='matching select files.',
        long_description='None',
        author='hinata',
        author_email='mail@hinatamasaki.com',
        url='',
        license='MIT',
        packages=find_packages(),
        install_requires=['click', 'pyperclip', 'termcolor'],
        entry_points={'console_scripts': [
            "matti = matti.matti:main"]
            }
        )

