import click
import pyperclip
import termcolor
import pathlib
import shutil


@click.command()
@click.option('--path', '-p', default='.', help="target path. default pwd.")
def main(path):
# get paste list(ignore suffix)
    org_pasteList  = pyperclip.paste().split()
    ignore_suffix = [ pathlib.Path(i).stem for i in org_pasteList ]
    checkList = ignore_suffix[:]
    checkSet = set(ignore_suffix)
# get file paths
    p = pathlib.Path(path).expanduser().resolve()
    targetFiles = p.glob('*')
# make select dir
    pathlib.Path('SELECT').mkdir()

# match 
    for i in targetFiles:
        if i.stem in checkSet:
            shutil.move(str(i), 'SELECT')
            try:
                checkList.remove(str(i.stem))
            except ValueError:
                pass
# if no match
    termcolor.cprint("Not match files. please check..", 'red')
    for i in checkList:
        print(i)


if __name__ == '__main__':
    main()
