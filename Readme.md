# matti  
  
複数の候補を一括でセレクトフォルダにぶち込みます。  
大量のアタリデータから選んでもらったOKカットリストを手作業で探す日々よさらば。。  
  
本スクリプトは以下が必要になります。  
  
1. homebrew(macOsのパッケージマネージャ)  
2. python3 (プログラミング言語。homebrewからインストールします。)  
  
  
  
## Installation  
  
### 1.homebrew https://brew.sh/index_ja  
  
macOsの「ターミナル」を開いて以下をコピペします。  
段落で分かれているかもしれませんがこれは一行で入力します。  
  
`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`  
  
### 2.python3 https://www.python.org/  
  
次に以下をコピペします。もともとmacOsに入っているpythonはバージョン2なので互換性はありません。  
  
`brew install python`  
  
### 3.python3がインストールされているか確認  
  
`python3 -V`と入力して`Python 3.7.3`と出れば正常にインストールされています。  
（3から後ろの数字は今後のバージョンアップに伴い変化していきます）  
  
### 4.matti  
  
本スクリプトのインストールはpipと呼ばれるライブラリ管理ソフトからインストールします。  
  
`pip3 install git+https://gitlab.com/hhnt/matti`
  
  
## Feature  
  
本スクリプトは以下の特徴を持ちます。  
  
- 指定したディレクトリのみを検索（デフォルはカレントディレクトリ）  
- SELECTフォルダが既にある場合はエラーになります。  
- ファイル名のみで検索します（拡張子は無視）  
  
  
  
## Usage  
  
example1  
  
```  
# チェックしたい箇所に移動する  
cd ~/Target/Dir  
  
# リストをコピペする  
ls ~/List/Dir | pbcopy  
  
# マッチングする  
matti  
```  
  
example2  
  
```  
# リストはコピペ済みとする  
# matti --path ~/Target/Dir  
```  
  
この場合は実行した時の場所にSELECTフォルダが作られる。  
  
